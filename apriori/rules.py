from itertools import combinations

class Rules:
    def __init__(self):
        self.__distinctAB = []
        
    def generateCandidateRules(self, listOfKItemset=[]):
        """
            Function is used to generate rules for all combination of K-itemsets for 
            in list passed to this function.
            Generates rules in following structure:
                listOfCandidateRules = [
                                        [{A},{B}, {support: 0, 'confidence':0}]
                                        [{A},{B}, {support: 0,'confidence':0}]             
                                    ]
            
            Paramter:
                listOfKItemset: (list) list of K itemset
                
            Return:
                listOfCandidateRules: (list) the generated rules are stored as above format
        """
        
        prop = {'support':0, 'confidence':0}
        listOfCandidateRules = []
        for itemset in listOfKItemset:
            
            # first element of the itemset is the key value pair and second is property such as support and confidence
            kvlength = len(itemset[0].keys())
            # logic to generate rules for two itemset
            if kvlength == 2:
                A, B = {}, {}
                for k, v in itemset[0].iteritems():
                    if len(A.keys()) == 0:
                        A[k] = v
                    else:
                        B[k] = v
                listOfCandidateRules.append([A, B, prop])
                # now swap A -> B to get B -> A
                listOfCandidateRules.append([B, A, prop])
            # logic ot generate rules for more than two itemset
            elif kvlength > 2:
                for i in range(1, kvlength):
                    # generate ith possible combination of itemset (dict) 
                    for keys_tuple in combinations(itemset[0], i):
                        A, B = {}, {}
                        for k, v in itemset[0].iteritems():
                            # add key value found in combination in A else in B
                            if k in keys_tuple:
                                A[k] = v
                            else:
                                B[k] = v
                        listOfCandidateRules.append([A, B, prop])
        return listOfCandidateRules
    
    def createDistinctABSetWithSupport(self, listOfCandidateRules=[], datasetObj=None):
        """
            This function is the optimization of frequent lookup in dataset for all possible
            A and B part of the rules in listOfCandidateRules. It avoid lookup in dataset 
            if support is available in self.__distinctAB list of dictionary.
            
            Store A or B in list of list of dictionary and check against this before inserting. 
            [[{k:v},{support,confidence}],[]]
            
            Parameter: 
                listOfCandidateRules: (list) list of rules generated using K-itemset
                datasetObj: (object) object of class Dataset
            
        """
        if datasetObj is None:
            raise ValueError("dataset object cannot be None")
        
        for candidates in listOfCandidateRules:
            # loop for two times to retrieve A and B from candidates 
            for i in range(0, 2):
                # search if key value pairs of itemset is pre-calculated
                support = self.__searchSupportInDistinctABSet(candidates[i])
                # if pre-computed support for itemset doesnot exists then search support in KItemlist
                if support is None:
                    support = self.__searchListOfKItemsetForSupport(candidates[i])
                    # if support doesnot exists then find in dataset.
                    if support is None:
                        freq = datasetObj.findFrequencyOfKeyValues(candidates[i])
                        support = float(freq) / datasetObj.getRowsCount()
                self.__distinctAB.append([candidates[i], {'support':support}])


    def getConfidenceOfCandidateRules(self, listOfCandidateRules=[]):
        """
            This function calculates confidence of each rules in listOfCandidateRules.
            
            Parameter:
                listOfCandidateRules: (list) list of rules for which confidence to be calculated.
            
            Return:
                newlist: (list) return the list of computed confidence.
        """
        newlist = []
        for candidates in listOfCandidateRules:
            # merge rule A --> B to generate Kitemset from which rule was generated
            temp = {}
            temp.update(candidates[0])
            temp.update(candidates[1])
            # rules are created from Kitemset so support can be retrieved directly from 
            # the list of k itemset. So called below function
            numerator = self.__searchListOfKItemsetForSupport(temp)
            # for support of A itemset , called below function
            denom = self.__searchSupportInDistinctABSet(candidates[0])
            confidence = round((float(numerator) / denom), 3)
            newlist.append([candidates[0],candidates[1],{'confidence':confidence, 'support':numerator}])
        return newlist
    
    def __searchSupportInDistinctABSet(self, itemset={}):
        """    
            Function to search itemset key-value pair in __distinctAB 
            
            Paramter:
                itemset: (dict) keyvalue pair of K Itemset to be search
                
            Return:
                support value or None 
        
        """
        for row in self.__distinctAB:
            try:
                # if not found then raises ValueError 
                row.index(itemset)
                return row[1].get('support', 0)
            except ValueError:
                pass
    
    def __searchListOfKItemsetForSupport(self, itemset={}):
        """
            This function is used to find itemset in Kitemset list and return support
            for all matched key-value pairs in itemset.
            
            Parameter:
                itemset: (dict) keyvalue pair to be searched 
            
            Return:
                support value or None
        """
        for row in self.__listOfKItemset:
            try:
                # if itemset not found then raises value error. 
                row.index(itemset)
                return row[1].get('support', 0) 
            except ValueError:
                pass
 
            
    def setListOfKItemsetForSupport(self, listOfKItemset=[]):
        if len(listOfKItemset) == 0:
            raise ValueError("List of K itemset cannot be empty.")
        self.__listOfKItemset = listOfKItemset
    

        
