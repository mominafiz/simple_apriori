
def userInterface():
    
    filename = raw_input("Enter Data file name: ")
    if filename == None:
        raise ValueError("Invalid filename.")
    
    support = raw_input("Enter Support Percentage: ")
    try:
        support = round(float(support),2)
    except Exception:
        raise ValueError("Cannot parse value. Invalid support percentage.")
    if support is None or support < 0.0 or support > 1.0:
        raise ValueError("Invalid support percentage.")
    
    
    confidence = raw_input("Enter Confidence Percentage: ")
    try:
        confidence = round(float(confidence),2)
    except Exception:
        raise ValueError("Cannot parse value. Invalid confidence percentage.")
    if confidence is None or confidence < 0.0 or confidence > 1.0:
        raise ValueError("Invalid confidence percentage.")
    
    
    return filename, support, confidence

def output():
    filename = raw_input("Enter output file name: ")
    if filename == None:
        raise ValueError("Invalid filename.")
    return filename