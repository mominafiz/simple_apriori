from ui import userInterface, output
from algorithm import Algorithm
from dataset import Dataset
from rules import Rules
import sys

if __name__ == "__main__":
    
    # Call function userInterface() of module ui.py. 
    # stores tuple of values returned from the function
    input_tuple = userInterface()
    # Create algorithm object and pass input paramters from user to constructor 
    algObj = Algorithm(input_tuple[0], input_tuple[1], input_tuple[2])
    # Create object of class Dataset
    dbObj = Dataset()
    # Dependency injection 
    algObj.loadDataset(dbObj)
    # call to function to generate one itemset 
    algObj.generateOneItemset()
    # prune One itemset
    listOfItemset = algObj.pruneKItemsetBySupport(algObj.getListOfOneItemset())
    # number of K-items generated.
    level = 1
    
    while True:
        # generate itemset of size == level
        listOfItemset = algObj.generateKItemset(level, listOfItemset)
        # break if no k-itemset can be generated.
        if len(listOfItemset) == 0:
            break
        # prune K itemset 
        listOfItemset = algObj.pruneKItemsetBySupport(listOfKItemset=listOfItemset)
        # break if no k-itemset is in the list after prunning
        if len(listOfItemset) == 0:
            break
        # finally add listOfItemset to listofKItemset where all possible K itemset is stored.
        extlist=algObj.getListOfKItemset()
        extlist.extend(listOfItemset)
        algObj.setListOfKItemset(extlist)
        
        level += 1
    
    # create object of Rules class
    rulesObj = Rules()
    # generate rules for each itemset in list of K itemset.
    listOfCandidateRules = rulesObj.generateCandidateRules(algObj.getListOfKItemset())
    # set list of K itemset for use in rules class
    rulesObj.setListOfKItemsetForSupport(algObj.getListOfKItemset())
    # create distinct A-->B set for optimization 
    rulesObj.createDistinctABSetWithSupport(listOfCandidateRules,dbObj)
    # calculate confidence for each rules in the list
    listOfCandidateRules = rulesObj.getConfidenceOfCandidateRules(listOfCandidateRules)
    # prunning by confidence
    listOfCandidateRules = algObj.pruneCandidateRulesByConfidence(listOfCandidateRules)
    algObj.setListOfCandidateRules(listOfCandidateRules)
    # call output function to ask user for output file.
    ofilename = output()
    # write output of rules in given file
    dbObj.writeOutputOfRules(ofilename, algObj)
    
    print("\nPlease check output file in 'data' folder under project directory. \n")
    print("End of the program.")
    sys.exit()
    
