
class Algorithm:
    def __init__(self, filename, support, confidence):
        """
            Constructor that initializes variables.
            
            parameters:
            filename: (string) takes file name of the dataset as the input parameter
            support: (float) user defined support value
            confidence: (float) user defined confidence value.
        """
        self.__filename = filename
        self.__support = support
        self.__confidence = confidence
        self.__dbObj = None
        self.__listOfOneItemset = []
        self.__listOfKItemset = []
        self.__itemsetLookup = {}
        self.__listOfCandidateRules = []
    
    def loadDataset(self, dbObj):
        """ Function to load dataset from user specified file and 
            checks for number of rows
        
            parameter:
            dbObj: (object) takes object of class Dataset of module dataset.py
        """
        self.__dbObj = dbObj
        self.__dbObj.load(filename=self.__filename)
        if self.__dbObj.getRowsCount() <= 0:
            raise Exception("Input file has no data.")
    
    def generateOneItemset(self):
        """    
            Function generates one itemset from dataset and sets default frequency and support to 0.
            Data is stored in following format in the list
            self.__listOfOneItemset:
                    [
                        [{'key1':value,'key2':value },{'frequency':0, 'support':0}], 
                        [{'key1':value,'key2':value },{'frequency':0, 'support':0}]
                    ]        
        """
        listOfColumns = self.__dbObj.getColumnHeaders()
        # iterate for number of columns in dataset
        for i in range(0, len(listOfColumns)):
            # iterate over rows in dataset except headers
            for row_tuple in self.__dbObj.getRecords():
                itemset, kv, kvproperties, colname = [], {}, {'frequency':0, 'support':0}, listOfColumns[i]
                # check if one itemset already exists in list.
                if self.__isDistinctItemset(colname, row_tuple[i]):
                    kv[colname] = row_tuple[i]
                    itemset.append(kv)
                    itemset.append(kvproperties)
                    self.__listOfOneItemset.append(itemset)
    
    def __isDistinctItemset(self,key,value):
        """
            private function that checks key-value pair in the dictionary.
            
            Parameter:
                key: (string) key of one itemset (column name)
                value: (string/number) value of one itemset (value of corresponding column)
            
            Return:
                True or False
        """
        if key not in self.__itemsetLookup:
            self.__itemsetLookup[key] = [value]
            return True
        elif key in self.__itemsetLookup and value not in self.__itemsetLookup.get(key):
            self.__itemsetLookup.get(key).append(value)
            return True
        else:
            return False
    
    def pruneKItemsetBySupport(self, listOfKItemset=[]):
        """
            This function is used to prune K-Itemset for support values less than user
            defined support value (0.0-1.0)
            
            Parameter:
                listOfKItemset: (list of list of dictionary) default parameter that accepts Itemset of size
                                one to K.
                                structure - [[{key-value pairs},{frequency, support}],[{},{}]]
            Return:
                pruned: (list) list of pruned input parameter.
        """
        pruned = []
        for kItemset in listOfKItemset:
            # finds frequency of key value pairs in dataset.
            frequency = self.__dbObj.findFrequencyOfKeyValues(kItemset[0])
            rows = self.__dbObj.getRowsCount()
            support = float(frequency) / rows
            support = round(support, 3)
            
            if support >= self.__support:
                kItemset[1]['frequency'], kItemset[1]['support'] = frequency, support
                pruned.append(kItemset)

        return pruned
    
    def pruneCandidateRulesByConfidence(self, listOfCandidateRules=[]):
        """
            Function prunes input listOfCandidateRules for confidence less than
            user defined confidence
            
            Parameter:
                listOfCandidateRules: (list of list of dictionary) 
                                    [[{A},{B},{support, confidence}],[{},{},{}]]
                                    
            Return:
                pruned: (list of list of dictionary) 
        """
        pruned = []
        for row in listOfCandidateRules:
            confidence = round(row[2].get('confidence',0),2)
            if confidence >= self.__confidence:
                pruned.append(row)
        return pruned       
        
    def generateKItemset(self, level=0, listOfKItemset=[]):
        """    
            Function generates k-itemset == level and return the list of k itemset.
            Create this prototype of the data listOfKItemset): 
                    [
                        [
                            {'key1':value,'key2':value },{'frequency':0, 'support':0}
                        ], 
                        [
                            {'key1':value,'key2':value },{'frequency':0, 'support':0}
                        ]
                    ]   
                    
            Parameter:
                level: (int) k value 
                listOfKItemset: (list) list of K Itemset == level -1 
                
            Return:
                listOfKplus1Itemset: (list) returns k+1 itemset. i.e itemset of length == level
        """
        listOfKplus1Itemset = []
        if level == 0:
            raise ValueError("Invoked incorrect function.")
        elif level > 0:
            totalKItemset = len(listOfKItemset)
            for i in range(0, totalKItemset-1):
                for j in range(i+1,totalKItemset):
                    itemset = []
                    kv, kvproperties = {}, {'frequency':0, 'support':0}
                    kv = self.joinItemset(level-1, listOfKItemset[i][0], listOfKItemset[j][0])
                    if kv is not None and not self.__isItemsetInKplus1Itemset(kv, listOfKplus1Itemset):
                        itemset.append(kv)
                        itemset.append(kvproperties)
                        listOfKplus1Itemset.append(itemset)
        else:
            raise ValueError("Invalid argument")
        
        return listOfKplus1Itemset
    
    def joinItemset(self, kvalue, outerItemset, innerItemset):
        """
            Use this function to merge two itemset to create k+1 itemset. 
            
            Parameter:
                kvalue: (int) the number of key:value pairs to be matched
                outerItemset: (dict) first itemset
                innerItemset: (dict) second itemset
            
            Return:
                matchPair: (dict) returns merged itemset
        """
        matchPair = {}
        # iterate each element in outerItemset and innerItemset
        for outerkey, outervalue in outerItemset.iteritems():
            if outerkey in innerItemset:
                # Check for same key different value i.e same group else matching key value pair
                if outervalue != innerItemset.get(outerkey):
                    return None
                else:
                    matchPair[outerkey] = outervalue
        
        # if matchPiar have key value pair equal to kvalue then return merged matchPair
        if len(matchPair.keys()) == kvalue:
            matchPair.update(outerItemset)
            matchPair.update(innerItemset)
        else:
            return None

        #if len(matchPair.keys()) == 0:
        #    return None
        return matchPair  
    
    def __isItemsetInKplus1Itemset(self, itemset, listOfKplus1Itemset):
        """
            Private function used to check if itemset exists in listOfKplus1Itemset.
            If itemset exists then return True else False
            
            Parameter:
                itemset: (dict) keyvalue pairs to be searched in listOfKplus1Itemset
                listOfKplus1Itemset: (list of list of dict) data structure to store k itemsets.
            
            Return:
                True or False depending on itemset exists.
        """
        itemset_len = len(itemset.keys())
        for row in listOfKplus1Itemset:
            count = 0
            for ik,iv in itemset.iteritems():
                if ik in row[0] and iv == row[0].get(ik):
                    count +=1
            # if matched count is equal to number of key-value paris in itemset
            if count == itemset_len:
                return True
        return False
            
    def setListOfCandidateRules(self, listOfCandidateRules=[]):
        self.__listOfCandidateRules = listOfCandidateRules
        
    def getListOfCandidateRules(self):
        return self.__listOfCandidateRules

    def getListOfOneItemset(self):
        return self.__listOfOneItemset
    
    def getListOfKItemset(self):
        return self.__listOfKItemset
    
    def setListOfKItemset(self,listOfKItemset=[]):
        self.__listOfKItemset = listOfKItemset
        
    def getSupport(self):
        return self.__support
    
    def getConfidence(self):
        return self.__confidence