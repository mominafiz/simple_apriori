
class Dataset:

    def __init__(self):
        """
            Constructor to initialize variables.
        """
        self.__columns = ()
        self.__records = []
        self.__lines = None
        self.__rows = 0

    def __createColumnHeaders(self):
        """
            private function to create headers tuple from read file. 
            This function assumes first line is header file and stores
            in object's variable.
        """
        if len(self.__lines) >= 1:
            columnstr = self.__lines[0]
            self.__columns = tuple(columnstr.split())
    
    def __createRecords(self):
        """
            Private function creates records, except header row and store row as tuple in list.
        """
        if len(self.__lines) < 1:
            raise Exception("No records in file.")
        # slice after 1st elemet in the list and store it in list  after
        # dividing it in columns.
        for line in self.__lines[1:]:
            if len(line.strip()) < 1:
                continue
            self.__records.append(tuple(line.split()))
            
    def __validateColumnsRecords(self):
        """
            Private function used to validate number of elements in each row
            is equal to number of elements of header row.
        """ 
        for record in self.__records:
            if len(record) != len(self.__columns):
                raise Exception("Column and record length mismatch.")

    def load(self,filename=""):
        """
            Used to store 
            This function accepts filename as the input parameter. It opens
            the file and read the contents.
            
            Parameter:
                filename: (string) accepts filename to read the dataset
        """
        try:
            # open file in read mode
            dbfile = open("../data/" + filename)
            # read all content of the file, each row as string and store in list
            self.__lines = dbfile.readlines()
            # call to private function to create column header  
            self.__createColumnHeaders()
            # call to private function to create rows of data except header.
            self.__createRecords()
            # validation
            self.__validateColumnsRecords()
            # save count of rows in dataset to avoid additional computation
            self.__rows = len(self.__records)
        except IOError:
            print("File not found!")
        except Exception:
            print("Error! Unexpected error occured while loading dataset.")

    def getRecords(self):
        return self.__records
    
    def getRowsCount(self):
        return self.__rows
    
    def getColumnHeaders(self):
        return self.__columns
    
    def findFrequencyOfKeyValues(self, itemset={}):
        """
            This function helps to find frequency of k-itemset from dataset
            
            Parameter:
                itemset: (dict) key:value pairs of items to be searched
            
            Return:
                frequency: (number/int) number of keyvalue pair found in dataset.
        """
        frequency=0
        no_of_keys = len(itemset.keys())
        for row_tuple in self.__records:
            # local_count: used to check all key:value pairs matches corresponding column's value of the row
            local_count = 0
            # iterate over key value pair of K itemset
            for k, v in itemset.iteritems():
                # find index of the column
                cindex = self.__columns.index(k)
                # iterate over records/rows to find match of key's value
                if row_tuple[cindex] == v:
                    local_count += 1
            # ensure number of keys in itemset is equal to matched values in current row
            if local_count == no_of_keys:
                frequency += 1
        return frequency
                    
    def __writeSummaryOfRules(self, rows, rules, support, confidence, ofile):
        """
            Private function that writes summary (i.e headers) in given file
            
            Parameter:
                rows: (int) total number of rows in dataset
                rules: (int) total number of rules discovered for given support and confidence
                support: (float) user defined support value
                confidence: (float) usr defined confidence value
                ofile: (file object) file in which summary has to be written.
        """
        ofile.write("Summary:")
        ofile.write("\n")
        ofile.write("Total rows in the original set: {0}".format(rows))
        ofile.write("\n")
        ofile.write("Total rules discovered: {0}\n".format(rules))
        ofile.write("The selected measures: Support={0} Confidence={1}\n".format(support,confidence))
        for i in range(100):
            ofile.write("-")
        ofile.write("\n")
        ofile.write("\n")
    
    def __writeRules(self, listOfCandidateRules, ofile):
        """
            Private function to write rules after summary of rules.
            
            Parameter:
                listOfCandidateRules: (object) contains rules A--> B, and support and confidence
                ofile: (file object) file in which rules to be written
        """
        ofile.write("Rules:")
        ofile.write("\n")
        ofile.write("\n")
        
        for i in range(1, len(listOfCandidateRules)+1):
            a = ""
            # generate format for A part of each rule and store it in 'a'
            for k,v in (listOfCandidateRules[i-1][0]).iteritems():
                a += "{0}={1} ".format(k,v)
            b = ""
            # generate format for B part of eah rule and store it in 'b'
            for k,v in (listOfCandidateRules[i-1][1]).iteritems():
                b += "{0}={1} ".format(k,v)
            # get support and confidence from the list
            properties = listOfCandidateRules[i-1][2]
            ofile.write("Rule#{0}: (Support={1}, Confidence={2})".\
                        format(i,round(properties.get("support"),2), \
                               round(properties.get("confidence"),2)))
            ofile.write("\n{ ")
            ofile.write(a.strip())
            ofile.write(" }")
            ofile.write("\n")
            ofile.write("------> ")
            ofile.write("{ ")
            ofile.write(b.strip())
            ofile.write(" }")
            ofile.write("\n")
            ofile.write("\n")
    
    def writeOutputOfRules(self, outputfilename, algorithmObj):
        """
            This function is called to write the final output to the file.
            
            Paramter: 
                outputfilename: (string) file to which output to be over-written
                algorithmObj: (obj) object of class Algorithm
        
        """
        candidateRules =  algorithmObj.getListOfCandidateRules()
        if outputfilename is None or len(outputfilename) ==0 or candidateRules is None:
            raise ValueError("Parameters cannot be None. Invalid values.")
        try:
            # open the file in write mode (it overwrites if file exists)
            ofile = open('../data/'+outputfilename, 'w')
            self.__writeSummaryOfRules(self.getRowsCount(), len(candidateRules),\
                                        algorithmObj.getSupport(), algorithmObj.getConfidence(), ofile)
            
            self.__writeRules(candidateRules, ofile)
            ofile.close()
        except IOError:
            print("Invalid output filename or directory does not exists.")