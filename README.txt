Author: 	Afiz Momin
-----------------------------------------------------------------------------------------------------------
 What does this program do?
	This program is written in python 2.7 and implements Apriori algorithm / association rules algorithm

 How to run the program?
	Program implements command line interface which asks user to enter input data file name,
	support (0 to 1), confidence (0 to 1), and output file name. 
	
	You need to open command prompt/terminal/shell.	Navigate to apriori package folder and type:
		python run.py

 Where will the output be stored?
	Output will be stored under 'data' folder of 'Apriori' project folder with a name given when the program is executed.

	
 How is project organized?
	Project folder is named 'Apriori'
	It has two folders:
		apriori: python package where source code files are stored
		data:	input and output file is stored
	
	apriori package has following modules:
		__init__.py:	package initialization file
		algorithm.py:	implementation of core apriori algorithm
		dataset.py:	implements functions for loading dataset, reading, searching, writing dataset in file
		rules.py:	implementation of association rules
		run.py		this file actually runs the program
		ui.py		implements user interface functions
		
 Function calls and implementation:
 	Detailed description of each function is documented in typical python documentation standard.
 	Please find it within the source code.
 	
 	ui.py			

                userInterface() 	functions ask user to input data file, support and confidence.
 		output() 		function ask user to input output rules file name.
 	
 	algorithm.py	)Apriori class):
		__init__ 			        constructor
		loadDataset()				loads dataset by calling load function of Dataset class
		generateOneItemset() 			creates a list of One itemset
		__isDistinctItemset() 			used by above method. private function to check if itemset already exists in the list
		pruneKItemsetBySupport()		removes itemset less than user specified support
		pruneCandidateRulesByConfidence() 	removes rules having confidence less than user defined confidence
		generateKItemset()	 		generates K Itemset until no possible K+1 Itemset created.
		joinItemset()				used by generate KItemset to join two itemset.
		__isItemsetInKplus1Itemset() 	        search if K + 1 Itemset already exists in list
						
	------ below are the getters and setters as name suggest --------
		setListOfCandidateRules()
		getListOfCandidateRules()
		getListOfOneItemset()
		getListOfKItemset()
		setListOfKItemset()
		getSupport()
		getConfidence()
 			
 	dataset.py	(Dataset class):
		__init__() 				constructor
		load()					loads data from file 
		__createColumnHeaders()			private function called from load() to create header list
		__createRecords()			private function called from load() to create records
		__validateColumnsRecords()		private function called from load() after above two methods to validate number of columns in records are equal to number of columns in the header	
		getRecords()				getter to get records, except header in file
		getRowsCount()				getter to get number of records
		getColumnHeaders()			getter to get columns of first line in file (headers)
		findFrequencyOfKeyValues()		returns the frequency of itemset/s matched; uses below func
		getRecords()				getter to get records, except header in file
		getColumnHeaders()			getter to get columns of first line in file (headers)
		writeOutputOfRules()			writes output of dataset to user defined output file
		__writeSummaryOfRules()			private function called by writeOutputOfRules() to write summary in the file.
		__writeRules()				private function called by writeOutputOfRules() to write rules in the file.
		getRowsCount()				getter to get number of records
						
 	
 	rules.py	(Rules class implements following methods):
 		__init__()				constructor
 		generateCandidateRules()		generates rules from K Itemset
 		createDistinctABSetWithSupport()	lookup for itemset and help read data from list created (optimization)
		__searchSupportInDistinctABSet()	checks if support for itemset is available in rule set 
		__searchListOfKItemsetForSupport()	checks if support for itemset is available in KItemset 
		findFrequencyOfKeyValues()		calls dataset.py file's function to calculate frequency
 		setListOfKItemsetForSupport()		setters method to set list of K Itemset
		getConfidenceOfCandidateRules()		calculates confidence using below function and returns the list of rules
		__searchSupportInDistinctABSet()	checks if support for itemset is available in rule set 
		__searchListOfKItemsetForSupport()	checks if support for itemset is available in KItemset 					
 					
 	run.py	(script file that runs the program.)
 		This script uses all above public functions and implements algorithm as explained in tutorial documentation.